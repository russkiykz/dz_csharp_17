﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkReflectionAndGC
{
    public class Person
    {
        public string FullName { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
    }
}
