﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkReflectionAndGC
{
    public abstract class TaskTwo
    {
        public static void ImplementationOfTaskTwo()
        {
            Console.WriteLine("Задание №2");
            Person man = new Person
            {
                FullName = "Иван Иванов",
                Age = 28,
                Gender = "Мужской",
                PhoneNumber = "87011236655"
            };

            foreach (var property in typeof(Person).GetProperties())
            {
                Console.WriteLine($"{property.Name} - {property.GetValue(man)}");
            }
        }
    }
}
