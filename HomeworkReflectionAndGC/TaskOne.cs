﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace HomeworkReflectionAndGC
{
    public abstract class TaskOne
    {
        public static void ImplementationOfTaskOne()
        {
            Console.WriteLine("Задание №1");
            foreach(var method in typeof(Console).GetMethods())
            {
                Console.WriteLine(method);
            }
        }
    }
}
