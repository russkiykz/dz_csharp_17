﻿using System;

namespace HomeworkReflectionAndGC
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskOne.ImplementationOfTaskOne();
            Console.WriteLine();
            TaskTwo.ImplementationOfTaskTwo();
        }
    }
}
